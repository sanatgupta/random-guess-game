#Random guessing game made by sanat gupta
#copyright @sanatg

import random
import math

class bcolors:

    BLUE = '\033[94m'
    GREEN = '\033[92m'
    RED = '\033[31m'
    YELLOW = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    BGRED = '\033[41m'
    WHITE = '\033[37m'

def logo():
    print(bcolors.YELLOW + bcolors.BOLD)
    print("""
 ____                 _                 
|  _ \ __ _ _ __   __| | ___  _ __ ___  
| |_) / _` | '_ \ / _` |/ _ \| '_ ` _ \ 
|  _ < (_| | | | | (_| | (_) | | | | | |
|_| \_\__,_|_| |_|\__,_|\___/|_| |_| |_|Guess Game

    """)
    print(bcolors.ENDC)	


logo()
print("Starting...")
print("Game Begins!!")
print("Hi,there welcome to the random guessing game")

#input variables
lower = int(input("Enter Lower Value:- "))

upper = int(input("Enter Upper Value:- "))


#generating random numbers
x = random.randint(lower,upper)
print("\n\tYou've only ",
       round(math.log(upper - lower + 1, 2)),
      " chances to guess the number!\n")

#initializing the guesses
count = 0

# minimum number of
# guesses depends upon range
while count < math.log(upper - lower + 1, 2):
	count += 1

	# taking guessing number 
	guess = int(input("Guess the number:- "))

	# Conditions
	if x == guess:
		print("Congo you did it in ",
			count, " try")
		#loop will break if guessed
		break
	elif x > guess:
		print("You guessed number is too small!")
	elif x < guess:
		print("You Guessed number is too big!")

# If guess increase the guesses provided,
# shows this output.
if count >= math.log(upper - lower + 1, 2):
	print("\nThe number is: %d" % x)
	print("\tBetter Luck Next time!")

#after the operation script will shutdown
print(bcolors.RED + bcolors.BOLD)
print("""
 ____  _   _ _   _ _____ _____ ___ _   _  ____ 
/ ___|| | | | | | |_   _|_   _|_ _| \ | |/ ___|
\___ \| |_| | | | | | |   | |  | ||  \| | |  _ 
 ___) |  _  | |_| | | |   | |  | || |\  | |_| |
|____/|_| |_|\___/  |_|   |_| |___|_| \_|\____|

    """)
print(bcolors.ENDC)	
