# Random Number Guess Game #

A random number guessing game in 

<img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=yellow"> 


### How to play? ###

* Enter upper value and lower value computer will generate a random integer between the range and will      store it for future references.
* you have to guess the number in a minimum number of guesses.
* good luck

### Starting the game? ### 
python3 main.py
### If you love my programming consider following me on -

[<img src="https://img.shields.io/badge/Github-000000?style=for-the-badge&logo=github&logoColor=white">](https://github.com/sanatg)
